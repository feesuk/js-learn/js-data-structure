let stack = [];

function pushStack(newStack) {
  // push element into the stack
  stack.push(newStack);
}

function popStack() {
  // return top most element in the stack
  // and removes it from the stack
  // Underflow if stack is empty
  if (stack.length === 0) return "Underflow if stack is empty";
  return stack.pop();
}

function peekStack() {
  // return the top most element from the stack
  // but does'nt delete it.
  console.log(stack[stack.length - 1]);
}

//Helper methods

function isStackEmpty() {
  // return true if stack is empty
  return stack.length === 0;
}

function printStack() {
  console.log(stack);
}

pushStack(5);
pushStack(10);
pushStack(20);
pushStack(5);
pushStack(10);
pushStack(20);
popStack();

peekStack();
printStack();
